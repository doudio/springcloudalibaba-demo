>  ### version

| 中间件   | 版本  |
| -------- | ----- |
| nacos    | 1.4.1 |
| Sentinel | 1.8.1 |

>  ### 相关模块

| 项目模块   | 作用  |
| -------- | ----- |
| gateway | 转发网关 |
| api | 内网API服务 |
| plugin-service    | 第三方平台对接服务 |
| data | 数据处理 |
| common | 基础公用包 |
| admin | 后台管理 |

> 微服务脚手架

* https://gitee.com/zuihou111/lamp-cloud

> 微服务版本管理

* https://github.com/alibaba/spring-cloud-alibaba/wiki/版本说明
* https://start.spring.io/actuator/info