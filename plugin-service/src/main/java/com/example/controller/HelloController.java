package com.example.controller;

import com.example.feign.dto.HelloDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RestController
public class HelloController {

    @Value("${config.info}")
    private String info;

    @GetMapping("/config")
    public String config() {
        return String.format("now: %s ,dyInfo %s", LocalDateTime.now(), info);
    }

    @PostMapping("/dto")
    public HelloDto dto(HelloDto dto) {
        log.info("b.dto: {}", dto);
        dto.setDesc(info);
        log.info("a.dto: {}", dto);
        return dto;
    }

}
