package com.example.plugin.web.parameter;

import com.example.plugin.web.enums.SystemEnum;
import com.example.plugin.web.result.ApiException;
import org.springframework.validation.BindingResult;

/**
 * 验证前端传入参数是否合法
 */
public class ParamUtils {

    public static void validation(BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            return;
        }
        /*StringBuilder stringBuilder = new StringBuilder();
        List<FieldError> allErrors = bindingResult.getFieldErrors();
        allErrors.stream().forEach(e -> {
            stringBuilder.append(e.getField()).append("::").append(e.getDefaultMessage());
        });*/
        throw new ApiException(SystemEnum.PARAM_ERROR);
    }

}
