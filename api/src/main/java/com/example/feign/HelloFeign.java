package com.example.feign;

import com.example.feign.dto.HelloDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(value = "plugin-service")
public interface HelloFeign {

    @GetMapping("/config")
    String config();

    @PostMapping(value = "/dto", consumes = "application/json")
    HelloDto dto(@SpringQueryMap HelloDto dto);

}
