package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.feign.HelloFeign;
import com.example.feign.dto.HelloDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloController {

    @Autowired
    private HelloFeign helloFeign;

    @Value("${server.port}")
    private String port;

    @GetMapping("/config")
    public String config() {
        return helloFeign.config().concat(" , api");
    }

    @GetMapping("/help")
    public String help() {
        return "api.help." + port;
    }

    @PostMapping("/dto")
    public String dto(HelloDto dto) {
        log.info("b: {}", dto);
        HelloDto helloDto = helloFeign.dto(dto);
        log.info("a: {}", dto);
        return JSON.toJSONString(helloDto) + port;
    }

}
