package com.example.feign.dto;

import lombok.Data;

@Data
public class HelloDto {

    private String name;
    private Integer ages;
    private String desc;

}
