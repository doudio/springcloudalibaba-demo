package com.example.util;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

public class ValUtils {

    public static boolean isNotBlank(final CharSequence cs) {
        return StringUtils.isNotBlank(cs);
    }

    public static boolean isBlank(final CharSequence cs) {
        return StringUtils.isBlank(cs);
    }

    public static boolean isEmpty(final Object object) {
        return ObjectUtils.isEmpty(object);
    }

    public static boolean isNotEmpty(final Object object) {
        return ObjectUtils.isNotEmpty(object);
    }

}
