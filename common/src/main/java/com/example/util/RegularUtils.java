package com.example.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularUtils {

    public static String matchOne(String target, String rex) {
        Pattern p = Pattern.compile(rex);
        Matcher matcher = p.matcher(target);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

}
