package com.example.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class IOUtils {

    public static String read(File file) {
        StringBuilder builder = new StringBuilder();
        try(BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));){
            String line = null;
            while((line = input.readLine()) != null) {
                builder.append(line);
                builder.append(String.format("%n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    public static void write(File file) {
        try(BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));){
            List<String> list = new ArrayList<>();
            for (String context : list) {
                output.write(String.format("%s%n", context));
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
