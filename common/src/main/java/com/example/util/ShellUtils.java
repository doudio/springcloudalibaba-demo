package com.example.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 调用操纵系统指令
 **/
public class ShellUtils {

    public static List<String> winsCmdOne(String commands) {
        List<String> resultList = new ArrayList<>();
        String[] cmd = {"cmd.exe", "/c", commands};
        simpleRun(cmd, resultList);
        return resultList;
    }


    public static List<String> linuxCmdOne(String commands) {
        List<String> resultList = new ArrayList<>();
        String[] cmd = {"/bin/sh", "-c", commands};
        simpleRun(cmd, resultList);
        return resultList;
    }

    private static void simpleRun(String[] cmd, List<String> resultList) {
        try {
            Process process = new ProcessBuilder().command(cmd).start();
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                resultList.add(line);
            }
            process.destroy();
            in.close();
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

}
